__author__ = 'tpaulus'

import json
import csv
from objects import *


class Load(object):
    @staticmethod
    def get_location(attribute, config_loc='./config.json'):
        with open(config_loc) as configuration_data:
            config = json.loads(configuration_data.read())

        return config['File Locations'][attribute]

    def __init__(self):
        self.photo_db_location = Load.get_location("Photo DB Location")
        self.quotes_location = Load.get_location("Quotes")
        self.student_list = self.make_students()

    def load_picture_db(self):
        with open('%s/INDEX.TXT' % self.photo_db_location) as prestige_db:
            student_portrait_list = list()
            line = prestige_db.readline()
            while line != '':
                split_line = line.split('\t')
                folder = split_line[1]
                file_name = split_line[2]
                last_name = split_line[4]
                first_name = split_line[5]
                new_student = StudentPortrait(first_name, last_name, self.photo_db_location, folder, file_name)

                student_portrait_list.append(new_student)

                line = prestige_db.readline()

        student_portrait_list.sort(key=lambda x: x.get_name_string())

        print 'Portrait DB Loaded'
        return student_portrait_list

    def load_quotes(self):
        student_quote_list = list()

        dictReader = csv.DictReader(open(self.quotes_location, 'rb'), fieldnames=['Last Name', 'First Name', 'Quote'],
                                    delimiter=',', quotechar='"')

        for row in dictReader:
            new_student_quote = StudentQuote(row['First Name'], row['Last Name'], row['Quote'])
            student_quote_list.append(new_student_quote)

        student_quote_list.pop(000)  # Remove Header Row
        student_quote_list.sort(key=lambda x: x.get_name_string())

        print 'Quotes DB Loaded'
        return student_quote_list

    def make_students(self):
        student_list = list()
        picture_db = self.load_picture_db()
        quote_db = self.load_quotes()

        for student_photo in picture_db:
            has_quote = False
            for student_quote in quote_db:
                if student_photo.get_name_string().replace(' ', '') == student_quote.get_name_string().replace(' ', ''):
                    new_student = Student(student_photo, student_quote)
                    student_list.append(new_student)

                    has_quote = True

            if not has_quote:
                name = student_photo['name']
                quote = 'Go Bulldogs!'
                new_student_quote = StudentQuote(name['first'], name['last'], quote)
                new_student = Student(student_photo, new_student_quote)
                student_list.append(new_student)

        return student_list


if __name__ == "__main__":
    l = Load()
    print l.student_list