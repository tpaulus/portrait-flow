A python script designed to take in Senior Photos (Prestige File Structure) as well as a CSV with Senior quotes and flow them into a set of Cards which are exported as PDFs.


Dependencies
=============

* ReportLab - Python Library for generating PDFs with Images and Flowables.

Usage
=============

* Ensure that photos are loaded in the folder specified in 'config.json' and that they are in the Prestige DB format
* Run 'flow.py' to output a PDF with the quotes and photos flowed with the selected layout output to the folder specified.