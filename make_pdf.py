__author__ = 'tpaulus'

import os
import json
import time
from reportlab.lib.pagesizes import letter
from reportlab.lib.units import inch
from reportlab.pdfgen import canvas
from reportlab.platypus import Image, Paragraph
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle, TA_CENTER
from reportlab.lib.colors import black
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont

from load import Load

# Remember the Origin is in the Bottom Left Corner3


class FlowPortraits(object):
    pdfmetrics.registerFont(TTFont('OS-Reg', './fonts/OpenSans-Regular.ttf'))
    pdfmetrics.registerFont(TTFont('OS-Light', './fonts/OpenSans-Light.ttf'))

    style_sheet = getSampleStyleSheet()

    style_sheet.add(ParagraphStyle(name='Quote', fontName='OS-Light', fontSize=12, alignment=TA_CENTER,
                                   textColor=black, backColor=None, allowOrphans=0, leading=13))

    style_sheet.add(ParagraphStyle(name='Quote-Small', fontName='OS-Light', fontSize=10, alignment=TA_CENTER,
                                   textColor=black, backColor=None, allowOrphans=0, leading=11))

    style_sheet.add(ParagraphStyle(name='Name', fontName='OS-Reg', fontSize=18, alignment=TA_CENTER,
                                   textColor=black, backColor=None, allowOrphans=0, leading=18))

    @staticmethod
    def get_layout_type(config_loc='./config.json'):
        with open(config_loc) as configuration_data:
            config = json.loads(configuration_data.read())

        return config['Flow Config']['Layout Number']

    @staticmethod
    def get_output_folder(config_loc='./config.json'):
        with open(config_loc) as configuration_data:
            config = json.loads(configuration_data.read())

        return config['File Locations']['PDF Export Location']

    def __init__(self):
        self.layout_type = self.get_layout_type()
        output_file = time.strftime('%Y%m%d-%H%M%S') + '.pdf'

        if not os.path.isdir(self.get_output_folder()):
            os.mkdir(self.get_output_folder())

        self.c = canvas.Canvas(self.get_output_folder() + '/' + output_file, pagesize=letter)

    def get_offset(self, position_number):
        with open('./layouts/%i.json' % self.layout_type) as layout_info:
            layout_specifications = json.loads(layout_info.read())
        position_info = layout_specifications['Positions'][position_number]

        return position_info['X'], position_info['Y']

    def get_module_specs(self):
        """
        Open the Layout JSON file and parse our the Module Specifications.
        :return: The Module Specification Dictionary
        :rtype: dict
        """
        with open('./layouts/%i.json' % self.layout_type) as layout_info:
            layout_specifications = json.loads(layout_info.read())
        mod_info = layout_specifications['Module']

        return mod_info

    def make_module(self, student, x, y):
        """
        Make the module which is placed on the page

        :param student:
        :type student: Student
        :param x: X offset of the Module
        :type x: int
        :param y: Y offset of the Module
        :type y: int
        :return: Portrait module with offsets X and Y
        """
        module_specs = self.get_module_specs()

        # Senior Portrait
        image = Image(student['portrait']['photo location'])
        image._restrictSize(module_specs['Photo']['Width'] * inch, module_specs['Photo']['Height'] * inch)
        image.drawOn(self.c, (module_specs['Photo']['X'] + x) * inch, (module_specs['Photo']['Y'] + y) * inch)

        # Name
        name = Paragraph(student['name'], style=FlowPortraits.style_sheet['Name'])
        name.wrapOn(self.c, module_specs['Name']['Width'] * inch, module_specs['Name']['Height'] * inch)
        name.drawOn(self.c, (module_specs['Name']['X'] + x) * inch, (module_specs['Name']['Y'] + y) * inch)

        # Quote
        quote = Paragraph(student['quote'], style=FlowPortraits.style_sheet['Quote'])
        quote.wrapOn(self.c, module_specs['Quote']['Width'] * inch, module_specs['Quote']['Height'] * inch)
        if quote.height > .75 * inch:
            quote = Paragraph(student['quote'], style=FlowPortraits.style_sheet['Quote-Small'])
            quote.wrapOn(self.c, module_specs['Quote']['Width'] * inch, module_specs['Quote']['Height'] * inch)
        quote.drawOn(self.c, (module_specs['Quote']['X'] + x) * inch, (module_specs['Quote']['Y'] + y) * inch)

    def make_page(self, students):
        """
        Actually make the PDF file

        :param students: List of Student Objects
        :type students: list
        """

        for page in range(0, len(students), self.layout_type):
            students_on_page = list()
            for student_number in range(page, page + self.layout_type):
                try:
                    students_on_page.append(students[student_number])
                except IndexError:
                    # End of File is not at the End of Page
                    # Nothing to Worry about!
                    pass

            current_position = 0
            for student in students_on_page:
                x, y = self.get_offset(current_position)
                self.make_module(student, x, y)

                current_position += 1

            self.c.showPage()  # showPage() creates a new page for all commands that come after showPage

        self.c.save()
        print 'PDF Generated'


if __name__ == "__main__":
    stu_list = Load().make_students()
    print 'DB Loaded'
    test_set = list()
    test_set_size = 1
    for i in range(0, test_set_size):
        test_set.append(stu_list[i])

    FlowPortraits().make_page(stu_list)
