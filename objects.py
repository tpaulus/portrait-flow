__author__ = 'tpaulus'


class StudentPortrait(object):
    def __init__(self, first_name, last_name, db_path, folder, file_name):
        """
        :param first_name: Student's First Name
        :type first_name: str
        :param last_name: Student's Last Name
        :type last_name: str
        :param db_path: Path to the Photos DB
        :type db_path: str
        :param folder: Output folder of the Photo (COLOR*)
        :type folder: str
        :param file_name: File Name from DB which is the Portrait of the Student
        :type file_name: str
        """
        self.name = {'first': first_name, 'last': last_name}
        self.photo_loc = '%s/%s/%s' % (db_path, folder, file_name)

    def __getitem__(self, item):
        if item == 'name':
            return self.name
        if item == 'photo location':
            return self.photo_loc
        else:
            attributes_dict = {'name': self.name, 'photo location': self.photo_loc}
            return attributes_dict

    def get_name_string(self):
        return self.name['last'] + ', ' + self.name['first']


class StudentQuote(object):
    def __init__(self, first_name, last_name, quote_text):
        """
        :param first_name: Student's First Name
        :type first_name: str
        :param last_name: Student's Last Name
        :type last_name: str
        :param quote_text: Student's Senior Quote
        :type quote_text: str
        """
        self.name = {'first': first_name, 'last': last_name}
        self.quote = quote_text

    def __getitem__(self, item):
        if item == 'name':
            return self.name
        if item == 'quote':
            return self.quote
        else:
            attributes_dict = {'name': self.name, 'quote': self.quote}
            return attributes_dict

    def get_name_string(self):
        return self.name['last'] + ', ' + self.name['first']


class Student(object):
    def __init__(self, portrait, quote):
        """
        :param portrait: Portrait Object for that Student
        :type portrait: StudentPortrait
        :param quote: Student's Senior Quote
        :type quote: StudentQuote
        """
        self.portrait = portrait
        self.quote = quote

    def __getitem__(self, item):
        if item == 'portrait':
            return self.portrait
        if item == 'quote':
            return self.quote['quote']
        if item == 'name':
            return self.get_name()

    def get_name(self):
        name_dict = self.portrait['name']
        name = '%s %s' % (name_dict['first'], name_dict['last'])

        return name
